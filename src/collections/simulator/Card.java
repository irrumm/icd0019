package collections.simulator;

import java.util.Objects;

public class Card implements Comparable<Card> {

    public enum CardValue { S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    private final CardValue value;
    private final CardSuit suit;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }

        Card other = (Card) obj;

        return Objects.equals(value, other.value)
                && Objects.equals(suit, other.suit);
    }

    @Override
    public int compareTo(Card other) {
        if (rankToInt(this.value) > rankToInt(other.value)) {
            return 1;
        }else if (rankToInt(this.value) != rankToInt(other.value)) {
            return -1;
        }
        return 0;
    }

    public CardValue getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }

    public static int rankToInt(CardValue rank) {
        String rankstr = rank.toString();
        if (rankstr.contains("S")) {
            return Integer.parseInt(rankstr.substring(1));
        } else if (rankstr.equals("T")) {
            return 10;
        } else if (rankstr.equals("J")) {
            return 11;
        } else if (rankstr.equals("Q")) {
            return 12;
        } else if (rankstr.equals("K")) {
            return 13;
        } else {
            return 14;
        }
    }
}
