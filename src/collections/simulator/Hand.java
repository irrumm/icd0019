package collections.simulator;

import java.util.*;
import java.util.stream.Collectors;

public class Hand implements Iterable<Card> {

    private List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        if (straightFlush(cards)) {
            return HandType.STRAIGHT_FLUSH;
        } else if (fourOfAKind(cards)) {
            return HandType.FOUR_OF_A_KIND;
        } else if (fullHouse(cards)) {
            return HandType.FULL_HOUSE;
        } else if (straight(cards)) {
            return HandType.STRAIGHT;
        } else if (flush(cards)) {
            return HandType.FLUSH;
        } else if (threeOfAKind(cards)) {
            return HandType.TRIPS;
        } else if (twoPair(cards)) {
            return HandType.TWO_PAIRS;
        } else if (onePair(cards)) {
            return HandType.ONE_PAIR;
        }
        return HandType.HIGH_CARD;
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    public boolean straight(List<Card> hand) {
        if (hand.size() < 5) {
            return false;
        }
        List<Integer> sortedValues = hand.stream()
                .map(x -> Card.rankToInt(x.getValue()))
                .sorted()
                .collect(Collectors.toList());

        for (int i = 0; i < hand.size() - 1; i++) {
            if (!sortedValues.get(i).equals(sortedValues.get(i + 1) - 1) && sortedValues.get(i + 1) != 14) {
                    return false;
            }
        }
        return true;
    }

    public boolean fourOfAKind(List<Card> hand) {
        if (hand.size() < 4) {
            return false;
        }
        for (int i = 0; i < hand.size() - 1; i++) {
            if (Card.rankToInt(hand.get(i).getValue()) != Card.rankToInt(hand.get(i + 1).getValue())) {
                return false;
            }
        }
        return true;
    }

    public boolean twoPair(List<Card> hand) {
        if (hand.size() < 4) {
            return false;
        }
        List<Card.CardValue> values = hand.stream()
                .map(Card::getValue)
                .collect(Collectors.toList());
        int pairs = 0;
        for (Card.CardValue value : values) {
            if (Collections.frequency(values, value) == 2) {
                pairs++;
            }
        }
        return pairs == 4;
    }

    public boolean threeOfAKind(List<Card> hand) {
        if (hand.size() < 3) {
            return false;
        }
        List<Card.CardValue> valueList = hand.stream()
                .map(Card::getValue)
                .collect(Collectors.toList());
        for (Card.CardValue value: valueList) {
            if (Collections.frequency(valueList, value) == 3) {
                return true;
            }
        }
        return false;
    }

    public boolean flush(List<Card> hand) {
        if (hand.size() < 5) {
            return false;
        }

        String suit = hand.get(0).getSuit().toString();
        for (int i = 1; i < hand.size(); i++) {
            if (!hand.get(i).getSuit().toString().equals(suit)) {
                return false;
            }
        }
        return true;
    }

    public boolean straightFlush(List<Card> hand) {
        return flush(hand) && straight(hand);
    }

    public boolean onePair(List<Card> hand) {
        if (hand.size() < 2) {
            return false;
        }
        List<Card.CardValue> values = hand.stream()
                .map(Card::getValue)
                .collect(Collectors.toList());
        int pairs = 0;
        for (Card.CardValue value : values) {
            if (Collections.frequency(values, value) == 2) {
                pairs++;
            }
        }
        return pairs == 2;
    }

    public boolean fullHouse(List<Card> hand) {
        return onePair(hand) && threeOfAKind(hand);
    }

}
