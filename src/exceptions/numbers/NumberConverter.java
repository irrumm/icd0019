package exceptions.numbers;

import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.io.*;

public class NumberConverter {

    String lang;
    File filePath;
    Properties properties;

    public NumberConverter(String lang) {
        this.lang = lang;
        this.filePath = new File(String.format("src/exceptions/numbers/numbers_%s.properties", lang));
        properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);

            properties.load(reader);

        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(lang, e);
        } catch (IOException e) {
            throw new MissingLanguageFileException(lang, e);
        } finally {
            close(is);
        }
    }

    public String numberInWords(Integer number) {

        for (int i = 1; i <= 9; i++) {
            if (!properties.containsKey(String.valueOf(i))) {
                throw new MissingTranslationException(String.valueOf(i));
            }
        }

        Integer remainder = number;
        String result = "";

        if (number >= 100) {
            result += properties.getProperty(String.valueOf(number.toString().charAt(0))) + properties.getProperty("hundreds-before-delimiter") + properties.getProperty("hundred");
            remainder = number % 100;
            if (remainder != 0) {
                result += properties.getProperty("hundreds-after-delimiter");
            }
        }

        if (10 < remainder) {
            result += tens(remainder);
            if (remainder < 20) {
                return result;
            }
            remainder = remainder % 10;
        }
        result += ones(remainder, number);
        return result;

    }

    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException ignore) {}
    }

    public String tens(int remainder) {

        Properties prop = new Properties();
        FileInputStream stream;

        try {
            stream = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);

            prop.load(reader);
        } catch (IOException e) {
            throw new MissingLanguageFileException(lang, e);
        }
        String res = "";

        if (10 < remainder && remainder < 20) {
            if (prop.containsKey(String.valueOf(remainder))) {
                res += prop.getProperty(String.valueOf(remainder));
            } else {
                res += prop.getProperty(String.valueOf(String.valueOf(remainder).charAt(1))) + prop.getProperty("teen");
            }
            return res;
        } else if (remainder >= 20) {
            if (prop.containsKey(String.valueOf(remainder).charAt(0) + "0")) {
                res += prop.getProperty(String.valueOf(remainder).charAt(0) + "0");
            } else {
                res += prop.getProperty(String.valueOf(String.valueOf(remainder).charAt(0))) + prop.getProperty("tens-suffix");
            }
        }
        return res;
    }

    public String ones(int remainder, int number) {

        var props = new Properties();

        FileInputStream is;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);

            props.load(reader);
        } catch (IOException e) {
            throw new MissingLanguageFileException(lang, e);
        }
        String result = "";

        if ((number > 10 && 100 > number || number > 100 &&
                Character.getNumericValue(String.valueOf(number).charAt(1)) >= 2) && number % 10 != 0) {
            result += props.getProperty("tens-after-delimiter");
        }
        if (number < 10 || remainder != 0) {
            result += props.getProperty(String.valueOf(remainder));
        }
        return result;
    }
}
