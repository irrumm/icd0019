package fp.sales;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Analyser {

    private Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        return repository.getFileList().stream()
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public Double getSalesByCategory(String category) {
        return repository.getFileList().stream()
                .filter(x -> x.getCategory().equals(category))
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        return repository.getFileList().stream()
                .filter(x -> x.getDate().isAfter(start) && x.getDate().isBefore(end))
                .map(Entry::getAmount)
                .mapToDouble(each -> each)
                .sum();
    }

    public String mostExpensiveItems() {
        return repository.getFileList().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Entry::getAmount)))
                .limit(3)
                .sorted(Comparator.comparing(Entry::getProductId))
                .map(Entry::getProductId)
                .collect(Collectors.joining(", "));
    }

    public String statesWithBiggestSales() {
         var map = repository.getFileList().stream().collect(
                Collectors.toMap(
                        Entry::getState,
                        Entry::getAmount,
                        Double::sum));
        return map.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(", "));
    }
}
