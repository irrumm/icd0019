package fp.sales;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");


    public List<Entry> getFileList() {
        try {
            List<String> filestring = Files.readAllLines(Paths.get(FILE_PATH));
            return filestring.stream()
                    .skip(1)
                    .map(x -> {
                        Entry entry = new Entry();
                        entry.setDate(LocalDate.parse(x.split("\t")[0], formatter));
                        entry.setState(x.split("\t")[1]);
                        entry.setProductId(x.split("\t")[2]);
                        entry.setCategory(x.split("\t")[3]);
                        entry.setAmount(Double.parseDouble(x.split("\t")[5].replace(",", ".")));
                        return entry;
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Problem occurred");
        }
        return null;
    }
}
