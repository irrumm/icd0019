package generics.cart;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart<T> {

    List<List<String>> items = new ArrayList<>();
    double cartPrice = 0.0;
    double lastDiscount = 0.0;

    public void add(CartItem item) {
        for (List<String> i: items) {
            if (i.get(0).equals(item.getId()) && i.get(1).equals(item.getPrice().toString())) {
                i.set(2, String.valueOf(Integer.parseInt(i.get(2)) + 1));
                return;
            }
        }
        List<String> itemInfo = new ArrayList<>();
        itemInfo.add(item.getId());
        itemInfo.add(String.valueOf(item.getPrice()));
        itemInfo.add("1");
        items.add(itemInfo);
    }

    public void removeById(String id) {
        items.removeIf(i -> i.get(0).equals(id));
    }

    public Double getTotal() {
        if (!items.isEmpty() && cartPrice == 0.0) {
            double sum = 0.0;
            for (List<String> i: items) {
                sum += Double.parseDouble(i.get(1)) * Double.parseDouble(i.get(2));
            }
            cartPrice = sum;
        }
        return cartPrice;
    }

    public void increaseQuantity(String id) {
        for (List<String> i: items) {
            if (i.get(0).equals(id)) {
                i.set(2, String.valueOf(Integer.parseInt(i.get(2)) + 1));
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        double discountPercentage = discount / 100;
        lastDiscount = discountPercentage;
        cartPrice = getTotal() * (1 - discountPercentage);
    }

    public void removeLastDiscount() {
        cartPrice = getTotal() / (1 - lastDiscount);
    }

    public void addAll(List<? extends CartItem> items) {
        for (CartItem i: items) {
            add(i);
        }
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < items.size(); i++) {
            result.append("(");
            for (int j = 0; j < 3; j++) {
                result.append(items.get(i).get(j));
                if (j != 2) {
                    result.append(", ");
                }
            }
            result.append(")");
            if (i != items.size() - 1) {
                result.append(", ");
            }
        }
        return String.valueOf(result);
    }
}
