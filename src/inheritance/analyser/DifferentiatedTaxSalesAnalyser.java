package inheritance.analyser;

import java.util.List;

public class DifferentiatedTaxSalesAnalyser extends SalesAnalyser{

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected double getTaxRate(SalesRecord item) {
        if (item.hasReducedRate()) {
            return 0.1;
        } else {
            return 0.2;
        }
    }
}
