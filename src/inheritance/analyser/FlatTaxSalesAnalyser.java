package inheritance.analyser;

import java.util.List;

public class FlatTaxSalesAnalyser extends SalesAnalyser{

    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected double getTaxRate(SalesRecord item) {
        return 0.2;
    }
}
