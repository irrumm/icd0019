package inheritance.analyser;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class SalesAnalyser {

    private final List<SalesRecord> records;
    public double taxRate;
    protected abstract double getTaxRate(SalesRecord item);

    public SalesAnalyser(List<SalesRecord> records){
        this.records = records;
    }

    public Double getTotalSales() {
        double result = 0;
        for (SalesRecord i: records){
            result += i.getProductPrice() * i.getItemsSold() / (1 + getTaxRate(i));
        }
        return result;
    }

    public Double getTotalSalesByProductId(String id) {
        double result = 0;
        for (SalesRecord i: records) {
            if (i.getProductId().equals(id)) {
                result += i.getProductPrice() * i.getItemsSold() / (1 + getTaxRate(i));
            }
        }
        return result;
    }

    public String getIdOfMostPopularItem() {
        SalesRecord current = records.get(0);
        for (SalesRecord i: records) {
            if (i.getItemsSold() > current.getItemsSold()) {
                current = i;
            }
        }
        return current.getProductId();
    }

    public String getIdOfItemWithLargestTotalSales() {
        SalesRecord most = records.get(0);
        double sum = records.get(0).getItemsSold();
        double current = sum;
        for (int i = 0; i < records.size() - 1; i++) {
            sum = records.get(i).getItemsSold() * records.get(i).getProductPrice() / (1 + getTaxRate(records.get(i)));
            for (int j = i + 1; j < records.size(); j++) {
                if (records.get(i).getProductId().equals(records.get(j).getProductId())) {
                    sum += records.get(j).getItemsSold() * records.get(j).getProductPrice() / (1 + getTaxRate(records.get(j)));
                }
            }
            if (sum > current) {
                current = sum;
                most = records.get(i);
            }
        }
        return most.getProductId();
    }

}
