package inheritance.analyser;

import java.util.List;

public class TaxFreeSalesAnalyser extends SalesAnalyser {

    @Override
    protected double getTaxRate(SalesRecord item) {
        return 0.0;
    }

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

}
