package junit;

public class Code {

    public static boolean isSpecial(int number) {
        return number % 11 <= 3;
    }

    public static int longestStreak(String input) {
        int len = input.length();
        if (len == 1) {
            return 1;
        }
        int count = 1;
        int result = 0;
        for (int i = 0; i < len - 1; i++) {
            if (input.charAt(i) == input.charAt(i+1)) {
                count++;
            } else {
                count = 1;
            }
            if (result < count) {
                result = count;
            }
        }
        return result;
    }

    public static Character mode(String input) {
        if (input == null) {
            return null;
        } else if (input.equals("")) {
            return null;
        } else {
            char max = input.charAt(0);
            for (char i : input.toCharArray()) {
                if (getCharacterCount(input, i) > getCharacterCount(input, max)) {
                    max = i;
                }
            }
            return max;
        }
    }

    public static int getCharacterCount(String input, char c) {
        int count = 0;
        for (char i : input.toCharArray()) {
            if (i == c) {
                count++;
            }
        }
        return count;
    }


    public static int[] removeDuplicates(int[] input) {
        if (input == null) {
            return null;
        } else if (input.length <= 1) {
            return input;
        } else {
            int[] result = new int[input.length];
            result[0] = input[0];
            int indexJ = 1;
            for (int i = 1; i < input.length; i++) {
                boolean isDuplicate = false;
                for (int j = 0; j < i; j++) {
                    if (input[j] == input[i]) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    result[indexJ++] = input[i];
                }
            }
            int[] finalresult = new int[nullCounter(input)];
            for (int a = 0; a < nullCounter(input); a++) {
                finalresult[a] = result[a];
            }
            return finalresult;
        }
    }

    public static int nullCounter(int[] input) {
        boolean isDuplicate = false;
        int numbers = 1;
        for (int i = 1; i < input.length; i++) {
            for (int j = 0; j < i; j++) {
                if (input[j] == input[i]) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                numbers += 1;
            } else {
                isDuplicate = false;
            }
        }
        return numbers;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        boolean isDuplicate = false;
        int result = integers[0];
        for (int i = 1; i <= integers.length - 1; i++) {
            for (int j = 0; j < i; j++) {
                if (integers[j] == integers[i]) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                result += integers[i];
            } else {
                isDuplicate = false;
            }
        }
        return result;
    }
    
}
