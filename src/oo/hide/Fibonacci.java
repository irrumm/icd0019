package oo.hide;

public class Fibonacci {

    private int next = 1;
    private int current = 0;


    public int nextValue() {
        int result = current;

        current = next;
        next = next + result;

        return result;
    }

}
