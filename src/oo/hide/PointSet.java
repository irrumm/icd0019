package oo.hide;


public class PointSet {

    private Point[] myArray;

    public PointSet(int capacity) {
        myArray = new Point[capacity];
    }

    public PointSet() {
        myArray = new Point[10];
    }

    public void add(Point point) {
        if (point == null) {
            return;
        }
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] != null) {
                if (myArray[i].x.equals(point.x) && myArray[i].y.equals(point.y)) {
                    return;
                }
            } else {
                myArray[i] = point;
                break;
            }
        }
    }

    public int size() {
        int answer = 0;
        for (Point i: this.myArray) {
            if (i != null) {
                answer++;
            }
        }
        return answer;
    }

    public boolean contains(Point point) {
        for (Point i: myArray) {
            if (i != null && i.x.equals(point.x) && i.y.equals(point.y)) {
                    return true;
            }
        }
        return false;
    }

    public String toString() {
        String result = "";
        String answer = "";
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] != null) {
                answer += result + String.format("(%s, %s)", myArray[i].x, myArray[i].y);

                if (i != myArray.length - 1 && myArray[i + 1] != null) {
                    answer += ", ";
                }

            } else {
                break;
            }
        }
        return answer;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PointSet)) {
            return false;
        }

        PointSet other = (PointSet) obj;
        for (Point i: myArray) {
            if (i != null && !other.toString().contains(i.toString())) {
                    return false;
            }
        }
        return true;
    }

    public PointSet subtract(PointSet other) {
        PointSet newPointSet = new PointSet();
        for(Point i: this.myArray) {
            if (i != null && !other.toString().contains(i.toString())) {
                    newPointSet.add(i);
            }
        }
        return newPointSet;
    }

    public PointSet intersect(PointSet other) {
        PointSet result = new PointSet();
        for (Point i : this.myArray) {
            if (i != null && other.toString().contains(i.toString())) {
                    result.add(i);
            }
        }
        return result;
    }
}
