package poly.customer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    public List<AbstractCustomer> customers = new ArrayList<>();

    public CustomerRepository() {
        try {
            List<String> customersString = Files.readAllLines(Paths.get(FILE_PATH));
            for (String s : customersString) {
                final String[] parts = s.split(";");
                if (customers.stream().anyMatch(x -> x.getId().equals(parts[2]))) {
                    continue;
                }
                if (parts[0].equals("REGULAR")) {
                        customers.add(new RegularCustomer(parts[1], parts[2], Integer.parseInt(parts[3]), LocalDate.parse(parts[4])));
                } else if (parts[0].equals("GOLD")) {
                        customers.add(new GoldCustomer(parts[1], parts[2], Integer.parseInt(parts[3]), LocalDate.now()));
                }

            }

        } catch (Exception e) {
            System.out.println("Something went wrong");
        }
    }

    public Optional<AbstractCustomer> getCustomerById(String id) {
        for (AbstractCustomer customer: customers) {
            if (customer.getId().equals(id)) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        customers.removeIf(x -> x.getId().equals(id));
        try {
            List<String> lines = Files.readAllLines(Paths.get(FILE_PATH));
            List<String> updatedLines = lines.stream()
                    .filter(s -> !s.contains(id)).collect(Collectors.toList());
            Files.write(Paths.get(FILE_PATH), updatedLines, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            System.out.println("Could not remove customer.");
        }

    }

    public void save(AbstractCustomer customer) {
        for (AbstractCustomer cus: customers) {
            if (cus.getId().equals(customer.getId())) {
                cus.bonusPoints = customer.bonusPoints;
                try {
                    Stream <String> lines = Files.lines(Paths.get(FILE_PATH));
                    List <String> replaced = lines.map(line -> line.replaceAll(
                            String.format("%s;%s;%s;", customer.getId(), customer.getName(), line.split(";")[3]),
                            String.format("%s;%s;%s;", customer.getId(), customer.getName(), customer.bonusPoints))).collect(Collectors.toList());
                    Files.write(Paths.get(FILE_PATH), replaced);
                    lines.close();
                } catch (IOException e) {
                    System.out.println("Unable to update customer.");
                }
                return;
            }
        }
        customers.add(customer);
        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(FILE_PATH, true));
            String text = "";
            if (customer instanceof RegularCustomer) {
                text = String.format("REGULAR;%s;%s;%s;%s\r\n", customer.getId(), customer.getName(),
                        customer.getBonusPoints(), ((RegularCustomer) customer).getLastOrderDate());
            }
            else if (customer instanceof GoldCustomer) {
                text = String.format("GOLD;%s;%s;%s;\r\n", customer.getId(), customer.getName(),
                        customer.getBonusPoints());
            }
            output.write(text);
            output.close();
        } catch (IOException e) {
            System.out.println("Unable to save customer.");
        }
    }

    public int getCustomerCount() {
        return customers.size();
    }
}
