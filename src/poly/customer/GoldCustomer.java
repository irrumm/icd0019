package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public class GoldCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;

    public GoldCustomer(String id, String name,
                        int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.lastOrderDate = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100) {
            bonusPoints += order.getTotal() * 1.5;
        }
        lastOrderDate = order.getDate();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints, lastOrderDate);
    }

    @Override
    public String asString() {
        return String.format("%s, %s, %s", id, name, bonusPoints);
    }

}