package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public class RegularCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.lastOrderDate = lastOrderDate;
    }

    public LocalDate getLastOrderDate() {
        return this.lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100) {
            if (order.getDate().isBefore(lastOrderDate.plusMonths(1))) {
                bonusPoints += order.getTotal() * 1.5;
            } else {
                bonusPoints += order.getTotal();
            }
        }
        lastOrderDate = order.getDate();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints, lastOrderDate);
    }

    @Override
    public String asString() {
        return String.format("%s, %s, %s", id, name, bonusPoints);
    }

}