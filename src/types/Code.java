package types;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9};

        System.out.println(sum(numbers)); // 11
    }

    public static int sum(int[] numbers) {
        int sum = 0;
        for (int i : numbers) {
            sum += i;
        }
        return sum;
    }

    public static double average(int[] numbers) {
        Double sum = 0.0;
        for (Integer number : numbers) {
            sum += number;
        }
        //noinspection UnnecessaryBoxing
        return Double.valueOf(sum / numbers.length);
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length != 0) {
            int minValue = integers[0];
            for (int i = 1; i <= integers.length - 1; i++) {
                if (integers[i] < minValue) {
                    minValue = integers[i];
                }
            }
            return minValue;
        }
        return null;
    }

    public static String asString(int[] elements) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i <= elements.length - 1; i++) {
            str.append(elements[i]);
            if (i != elements.length - 1) {
                str.append(", ");
            }
        }
        return str.toString();
    }

    public static String squareDigits(String s) {
        String result = "";
        for (char i : s.toCharArray()) {
            if (Character.isDigit(i)) {
                result += Character.getNumericValue(i) * Character.getNumericValue(i);
            } else {
                result += i;
            }
        }
        return result;
    }

}
